# mkdir node_modules && npm install csv-parse --save

# java -jar node_modules/google-closure-compiler/compiler.jar --js index.js --js_output_file index-min.js
# java -jar node_modules/google-closure-compiler/compiler.jar --compilation_level ADVANCED_OPTIMIZATIONS --js index.js --js_output_file index-min.js

# docco *.coffee

fs = require 'fs'
parse = require 'csv-parse'

fs.readFile './obliquestrategies.csv', {encoding: 'utf-8'}, (err,data) ->
  parse data, {}, (err, output) ->
    _print output[_randomNum 568, 2][2]




_randomNum = (max,min=0) ->
  return Math.floor(Math.random() * (max - min) + min)


_print = (str) ->
    console.log "\x1b[40m" + Array(str.length + 7).join "="
    console.log "== \x1b[4" + _randomNum(6, 1) + "m" + str + "\x1b[40m =="
    console.log Array(str.length + 7).join "="
